### Hi 👋, I am Victor Shermon
<hr/>

### 🏷️ About me
<p>
  Hi, I am Victor, i currently live in brazil. I've be working as Front-End and Back-End Developer since 2020.
</p>
<p>
  in my free time i like to play (and watch) soccer 🏈, to watch movies (🎥) and series (📺), to read books 📚, to listen to music 🎵 and eventually i like to philosophize about   life with a beer (🍺).
</p>

### 🏷️ My Skills
<p>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original-wordmark.svg" alt="node" width="40" height="40" /> &nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg" alt="javascript" width="40" height="40" />
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40" />&nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40" />&nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40" /> &nbsp;&nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" alt="html5" width="40" height="40" /> &nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" alt="css3" width="40" height="40" /> &nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nextjs/nextjs-original-wordmark.svg" width="40" height="40" title="NextJs" /> &nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original-wordmark.svg" width="40" height="40" title="ReactJs" /> &nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/docker/docker-original.svg" width="40" height="40" title="Docker"/> &nbsp; &nbsp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jest/jest-plain.svg" width="40" height="40" title="Jest" />
<p/> <br/> <br />


[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=anuraghazra&layout=demo)](https://github.com/anuraghazra/github-readme-stats)
